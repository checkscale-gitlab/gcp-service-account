# GCP Service Account module
[![changelog](https://img.shields.io/badge/changelog-Keep%20a%20Changelog%20v1.1.0-orange)](https://gitlab.com/WizDevOps/terraform-modules/gcp-service-account/blob/master/CHANGELOG.md)
[![telegram](https://img.shields.io/badge/chat-kubernetes_en-blue?style=flat&logo=telegram)](https://t.me/kubernetes_en)

Create a GCP Service Account and assign Roles to it

## Required variables

| Variable | Type | Description |
| -------- | ---- | ----------- |
| `project_id` | `string` | GCP Project ID |
| `name` | `string` | Service Account name |
| `roles` | `list` | Assign roles |

## Optional variables

| Variable | Description |
| -------- | ----------- |
| `generate_key` | Toggle GSA private key generation |
| `gke_workload_identity_sa` | Grant GSA Google API permissions; format documented in variable description |
| `description` | Details about the Service Account |

## Output

| Variable | Description |
| -------- | ----------- |
| `email` | Service Account email that will be used throughout the plans |
| `key` | Service Account Private Key |

## Usage

```hcl
module "gke_service_accounts" {
  source = "git::https://gitlab.com/WizDevOps/terraform-modules/gcp-service-account.git?ref=v0.1.0"

  project_id  = local.project_id
  name        = "sample-svacc"
  description = "Created by Terraform for the GKE Nodepool"
  service_account_roles = [
    "roles/logging.logWriter",
    "roles/monitoring.metricWriter",
    "roles/monitoring.viewer",
    "roles/stackdriver.resourceMetadata.writer",
  ]
  gke_workload_identity_sa = [
    "my-gcp-project.svc.id.goog[k8s-namespace/ksa]",
  ]
}
```
